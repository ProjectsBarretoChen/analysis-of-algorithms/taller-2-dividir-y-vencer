# !/bin/bash

# Usage: create a sequence of experiments like:

for i in `seq 0 500 30000`;do \
    echo -n $i && echo -n " " && \
    python3 test.py $i; done > plots.txt

# then use "plots.txt" to draw the execution times (in seconds).
# Here is an example using gnuplot:

gnuplot -persist -e "set grid; plot 'plots.txt' using 1:2 with linespoints title 'MaxDIfferm_1'"