## =========================================================================
## @author 
## Leonardo Florez-Valencia (florez-l@javeriana.edu.co)
##
## Modificado por:
## Juan Sebastián Barreto Jimenez (juan_barreto@javeriana.edu.co)
## Janet Chen He (j-chen@javeriana.edu.co)
## =========================================================================

'''
Usage: create a sequence of experiments like:

for i in `seq 0 1000 30000`;do \
  echo -n $i && echo -n " " && \
  python prueba.py $i; done > plots.txt
  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

then use "plots.txt" to draw the execution times (in seconds).
Here is an example using gnuplot:

plot "plots.txt" using 1:2 with linespoints title "maxDifferm_C1", \
     "plots.txt" using 1:3 with linespoints title "maxDifferm_C2", \
'''

import sys, random, time
from codeOne import maxDifferm_C1
from codeTwo import maxDifferm_C2

## -------------------------------------------------------------------------
def ExecAlgorithm(f, A, n = 5):
  t = float( 0 )
  for i in range( n ):
    S = A.copy( )
    st = time.time( )
    f( S )
    t += float( time.time( ) - st )
  # end for
  return t / float( n )
# end def

## -------------------------------------------------------------------------
o = 1000000
n = int( sys.argv[ 1 ] )
S = [ random.randint( -o, o ) for i in range( n ) ]
caso1 = ExecAlgorithm( maxDifferm_C1, S )
caso2 = ExecAlgorithm( maxDifferm_C2, S )

print( f"{caso1:.3e} {caso2:.3e}" )

## eof - prueba.py
