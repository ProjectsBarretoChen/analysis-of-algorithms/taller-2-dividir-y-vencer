import csv
from codeOne import maxDifferm_C1 as maxDifferm

filename_in = "test_cases.in"
filename_out = "test_cases.out"

with open(filename_in, "r") as fileIn:
    with open(filename_out, "r") as fileOut:
        reader_in = csv.reader(fileIn, delimiter=" ")
        reader_out = csv.reader(fileOut, delimiter=" ")
        num_lines = int(next(reader_in)[0])
        fails = []
        flag_fail = False
        decimals = 12
        for i in range(num_lines):
            test_cases = list(map(float, next(reader_out)))
            position_out = int(test_cases[0])
            difference_out = test_cases[1]
            lines = next(reader_in)
            lines.pop()
            S = list(map(float, lines))
            difference,position = maxDifferm(S)
            if round(difference_out - difference, decimals) != 0 or position != position_out:
                fails.append(i)
                flag_fail = True
        if(flag_fail):
            print("Fail in",fails)
        else:
            print("All cases function with a precision of",decimals,"decimals")
