\documentclass[oneside,spanish]{amsart}
\usepackage[T1]{fontenc}
\usepackage{float}
\usepackage{amstext}
\usepackage{amsthm}
\usepackage{amssymb}
\PassOptionsToPackage{normalem}{ulem}
\usepackage{ulem}
\usepackage{graphicx, color}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Some new commands
\newcommand{\noun}[1]{\textsc{#1}}
\floatstyle{ruled}
\newfloat{algorithm}{tbp}{loa}
\providecommand{\algorithmname}{Algoritmo}
\floatname{algorithm}{\protect\algorithmname}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\numberwithin{equation}{section}
\numberwithin{figure}{section}
\theoremstyle{definition}
\newtheorem*{defn*}{\protect\definitionname}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{xmpmulti}
\usepackage{algorithm,algpseudocode}

\makeatother

\usepackage{babel}
\addto\shorthandsspanish{\spanishdeactivate{~<>}}

\providecommand{\definitionname}{Definición}

\begin{document}
\title{Taller 2.1: Diseño y escritura ``dividir y vencer''}
\author{Juan Sebastián Barreto Jiménez $^1$ \hspace{1cm} Janet Chen He$^1$}
\date{\today	
\\
$^1$Departamento de Ingeniería de Sistemas, Pontificia Universidad           Javeriana\\Bogotá,  Colombia \\
      \texttt{\{juan\_barreto, j-chen\}@javeriana.edu.co}\\~\\}
\begin{abstract}
En este documento se presenta el desarrollo del taller 2, primera parte, que consta de resolver un ejercicio de manera iteractiva y con dividir y vencer. El ejercicio tiene como problema, encontrar la máxima i-diferencia de una secuencia.
\end{abstract}
\maketitle

\part{Análisis y diseño del problema}

\section{Análisis}

\subsection{Ejercicio 1: Iterativo}
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.7]{analisisTaller2_1.png}
    \caption{análisis}
    \label{fig:análisis}
\end{figure}

Se tiene que dada una secuencia de elementos, se quiere encontrar la máxima i-diferencia, para ello se presenta el análisis con respecto a la figura 1.1.

\medskip
\noindent
Con ello, se puede deducir entonces que mientras por un lado los elementos disminuye, por el otro aumenta. Por lo que se concluyo, que teniendo la suma total de la secuencia, se puede realizar operaciones matemáticas para obtener los valores de cada lado y de esta forma restar cada caso y obtener como resultado, el valor mayor.
Se tiene entonces que la operación matemática a cumplir debe ser:
\[
(sumT - aux) - (aux - S_i)
\]
En donde, aux, es una variable que inicialmente tiene el valor total de la suma, pero en cada interacción es necesario ir quitando el valor i-esima en donde se encuentra.
\[
aux = aux - S_i
\]

\subsection{Ejercicio 1: Dividir y vencer}
Para este segundo caso, se pensó en una solución muy similar, pero dividiendo los datos. Para esto, se aplico el dividir y vencer del algoritmo de sumatoria y adicional se tiene un algoritmo recurrente para que en cada caso solo se analice desde el pivote hasta el valor final. Ya que al igual que en primer caso, se concluyo que no es necesario recorrer los lados desde que se asigna un pivote, sino que se puede simplificar con una operación matemática.
En este caso se aplicaría:
\[
(SumT - right - S_b) - (right) 
\]
Es muy similar al caso anterior, sin embargo, acá la variable right no es un acumulador sino es la variable que almacenaría la suma desde el pivote más uno hacia la derecha. Por lo que no necesariamente se debe tener una iteración secuencial para obtener el valor correcto. 

\section{Diseño}

\subsection{Ejercicio 1 - Iterativo}
\begin{defn*}
Entradas:
\begin{enumerate}
\item $S_{i} \in \mathbb R$, secuencia de elementos, que pertenece a los reales. 
\end{enumerate}
\end{defn*}
~~~~
\begin{defn*}
Entradas de la función auxiliar del dividir y vencer:
\begin{enumerate}
\item $S_{i} \in \mathbb R$, secuencia de elementos, que pertenece a los reales. 
\item $b \in \mathbb N$, valor que indica la posición inicial de la secuencia, que pertenece a los naturales.
\item $e \in \mathbb N$, valor que indica la posición final de la secuencia, que pertenece a los naturales.
\item $difference \in \mathbb R$, valor máximo i-diferencia de la secuencia, que pertenece a los reales.
\item $pos \in \mathbb N$, valor de la posición de la máxima i-diferencia de la secuencia, que pertenece a los naturales.
\item $sumTotal \in \mathbb R$, valor de la suma total de la secuencia, que pertenece a los reales.
\end{enumerate}
\end{defn*}
\begin{defn*}
Salidas: \\
Se define una tupla con las siguientes variables:
\begin{enumerate}
\item $difference \in \mathbb R$, valor máximo i-diferencia de la secuencia, que pertenece a los reales.
\item $pos \in \mathbb N$, valor de la posición de la máxima i-diferencia de la secuencia, que pertenece a los naturales.
\end{enumerate}
\end{defn*}
~~~~~

\part{Algoritmos}

\section{Ejercicio: Caso 1}

\subsection{Algoritmo Iterativo}

Este algoritmo consta de encontrar la máxima i-diferencia de una secuencia, de manera iterativa.

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{MaxDifferm}{$S : S_{i} \in \mathbb R$}
  \State$sumT\leftarrow0$
  \State$difference\leftarrow-inf$
  \State$pos\leftarrow 0$
  \For{$i \in |S|$}
    \State$sumT\leftarrow sumT + S[i]$
  \EndFor

  \State$aux\leftarrow sumT$

  \For{$j \in |S|$}
    \State$value\leftarrow (sumT - aux) - (aux - S[j])$
    \State$aux\leftarrow aux - S[j]$
    \If{$difference < value$}
        \State$difference\leftarrow value$
        \State$pos\leftarrow j$
    \EndIf
  \EndFor
\State\Return{$[difference,pos]$}
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Iterativo}
\end{algorithm}

\subsection{Complejidad}
\begin{itemize}
\item El algoritmo \noun{MaxDifferm} tiene orden de complejidad temporal $ T(N)= O\left( n\right)$, calculado a partir de la inspección del código.
\item El algoritmo \noun{MaxDifferm} tiene orden de complejidad espacial $S(N)= 6$, calculado a partir de la inspección del código.
\end{itemize}
\subsection{Invariante}

\begin{itemize}
\item La variable difference siempre tendrá  el valor máximo i-diferencia de la secuencia.
\item Con la suma total y la posición en donde se calcula la i-diferencia, se puede obtener los valores hacia la izquierda y hacia la derecha.
\end{itemize}

\section{Ejercicio : Caso 2}

\subsection{Algoritmo Dividir y vencer}

Este algoritmo consta de encontrar la máxima i-diferencia de una secuencia, con dividir y vencer.

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{MaxDifferm}{$S : S_{i} \in \mathbb R$}
    \State$sumTotal\leftarrow \Call{Sum}{S,1}$
  \State\Return{MaxDiffermAux(S,1,|S|,-inf,-1,sumTotal)}
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Dividir y vencer}
\end{algorithm}

\begin{algorithm}[H]
\begin{algorithmic}[1]


\Procedure{MaxDiffermAux}{$S : S_{i} \in \mathbb R, b \in \mathbb N, e \in \mathbb N, difference \in \mathbb R, pos \in \mathbb N, sumTotal \in \mathbb R$ }
    \If{$b>e$}
        \State\Return{$[difference,pos]$}
    \Else
        \State$right\leftarrow \Call{Sum}{S,b+1}$
        \State$value\leftarrow (sumTotal - right-S[b]) - rigth $
        \If{$difference < value$}
            \State$difference \leftarrow value$
            \State$pos \leftarrow b$
        \EndIf
        \State\Return{\Call{MaxDiffermAux}{$S,b+1,e,difference,pos$}}
    \EndIf
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Dividir y vencer - Aux}
\end{algorithm}

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{Sum}{$S : S_{i} \in \mathbb R, b \in \mathbb N$ }
    \State\Return{SumAux(S,b,|S|)}
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Sum}
\end{algorithm}

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{SumAux}{$S : S_{i} \in \mathbb R, b \in \mathbb N, e \in \mathbb N$ }
  \If{b>e}
    \State\Return{0}
  \Else
    \State$q\leftarrow \lfloor (b+e) \div 2 \rfloor$
    \State\Return{SumAux(S,b,q-1) + S[q] + SumAux(S,q+1,e)}
  \EndIf
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - SumAux}
\end{algorithm}

\subsection{Complejidad}
\begin{itemize}
\item El algoritmo \noun{MaxDifferm} tiene orden de complejidad temporal según el teorema del maestro.
\[
T(N) =  aT(\frac{N}{b}) + f(n)
\]
En donde:
\[
a = 1
\]
Porque se invoca dos veces la recurrencia, sabiendo que puede tomar distinta sentencia según la condicional. (Línea 11).
\[
b = 2
\]
Puesto que los datos se van reduciendo en dos datos y se toma solo lo de la derecha. (Línea 11). 
\[
f(n) = n
\]
Complejidad de lo que no es recurrente, que corresponde a la suma con dividir y vencer:
\[
T(N) \in \Theta(n)
\]
Teniendo entonces la siguiente función a analizar:
\[
T(N) =  1T(\frac{N}{2}) + f(n)
\]
Por lo que se puede concluir:
\[
T(N) \in \Theta(log_2{n})
\]
Cumpliendo con el caso 2, del teorema del maestro.

\item El algoritmo \noun{MaxDifferm} tiene orden de complejidad espacial $S(N)= 2$, calculado a partir de la inspección del código.
\end{itemize}
\subsection{Invariante}

\begin{itemize}
\item La variable difference siempre tendrá  el valor máximo i-diferencia de la secuencia.
\item Con la suma total y la suma del valor desde el pivote más uno hacia la derecha, se puede obtener el valor de la secuencia hacia la derecha del pivote.
\end{itemize}

\part{Comparación de los algoritmos}

\begin{table}[H]
\begin{center}
\begin{tabular}{| c | c |}
\hline
Complejidad & MaxDifferm\\ \hline
Iterativo & $O\left( n\right)$  \\ \hline
Dividir y vencer &  $\Theta(log_2{n})$ \\ \hline
\end{tabular}
\caption{Tabla de Comparación de Algoritmos máxima i-diferencia}
\label{tab:coches}
\end{center}
\end{table}
\end{document}