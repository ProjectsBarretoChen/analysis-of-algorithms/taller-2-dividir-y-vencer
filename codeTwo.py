## =========================================================================
## @author 
## Juan Sebastián Barreto Jimenez (juan_barreto@javeriana.edu.co)
## Janet Chen He (j-chen@javeriana.edu.co)
## =========================================================================

import math
import numpy as np

## -------------------------------------------------------------------------
'''
sum: Invoca la función sumAux para sumar con "dividir-y-vencer"
@inputs:    S, secuencia de elementos, que pertenece a los reales.
            b, valor que indica la posición inicial de la secuencia, que pertenece a los naturales.
@outputs: valor que contiene la suma de toda la secuencia, que pertenece a los reales.
'''
def sum(S,b):
    return sumAux(S,b,len(S)-1)
# end def num

## -------------------------------------------------------------------------
'''
sumAux: Sumar con "dividir-y-vencer"
@inputs:    S, secuencia de elementos, que pertenece a los reales.
            b, valor que indica la posición inicial de la secuencia, que pertenece a los naturales.
            e, valor que indica la posición final de la secuencia, que pertenece a los naturales.
@outputs: valor que contiene la suma de la secuencia desde la posición b hasta la e, que pertenece a los reales.
'''
def sumAux(S,b,e):
    if(b>e):
        return 0
    else:
        q = math.floor((b+e)/2)
        return sumAux(S,b,q-1) + S[q] + sumAux(S,q+1,e)
    # end if
# end def sumAux

## -------------------------------------------------------------------------
'''
maxDifferm_C2: Invoca la función maxDiffermAux para encontrar la i-diferencia máxima con "dividir-y-vencer"
@inputs: S, secuencia de elementos, que pertenece a los reales.
@outputs:   difference, valor máximo i-diferencia de la secuencia, que pertenece a los reales.
            pos, valor de la posición de la máxima i-diferencia de la secuencia, que pertenece a los naturales.
'''
def maxDifferm_C2(S):
    sumTotal = sum(S,0)
    return maxDiffermAux(S,0,len(S)-1,-np.inf,-1,sumTotal)
# end def maxDifferm_C2
    
## -------------------------------------------------------------------------
'''
maxDiffermAux: Encuentra la i-diferencia máxima con "dividir-y-vencer"
@inputs:    S, secuencia de elementos, que pertenece a los reales.
            b, valor que indica la posición inicial de la secuencia, que pertenece a los naturales.
            e, valor que indica la posición final de la secuencia, que pertenece a los naturales.
            difference, valor máximo i-diferencia de la secuencia, que pertenece a los reales.
            pos, valor de la posición de la máxima i-diferencia de la secuencia, que pertenece a los naturales.
            sumTotal, valor que contiene la suma de toda la secuencia, que pertenece a los reales.
@outputs:   difference, valor máximo i-diferencia de la secuencia, que pertenece a los reales.
            pos, valor de la posición de la máxima i-diferencia de la secuencia, que pertenece a los naturales.
'''
def maxDiffermAux(S,b,e,difference,pos,sumTotal):
    if(b>e):
        return [difference,pos]
    else:
        right = sum(S,b+1)
        value = (sumTotal - right - S[b]) - right
        if(difference < value):
            difference = value
            pos = b
        return maxDiffermAux(S,b+1,e,difference,pos,sumTotal)
        # end if
    # end if
# end def maxDifferm_C2

## eof - codigo2.py