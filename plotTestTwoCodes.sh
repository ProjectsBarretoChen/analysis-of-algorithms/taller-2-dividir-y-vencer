# !/bin/bash

# Usage: create a sequence of experiments like:

for i in `seq 0 50 990`;do \
    echo -n $i && echo -n " " && \
    python3 testTwoCodes.py $i; done > plotsTwo.txt

# then use "plots.txt" to draw the execution times (in seconds).
# Here is an example using gnuplot:

gnuplot -persist -e "set grid; plot 'plotsTwo.txt' using 1:2 with linespoints title 'MaxDIffermC1', 'plotsTwo.txt' using 1:3 with linespoints title 'MaxDiffermC2;'"