## =========================================================================
## @author 
## Juan Sebastián Barreto Jimenez (juan_barreto@javeriana.edu.co)
## Janet Chen He (j-chen@javeriana.edu.co)
## =========================================================================

import math

## -------------------------------------------------------------------------
'''
sum: Sumar con "dividir-y-vencer"
@inputs:    S, secuencia de elementos, que pertenece a los reales.
            b, valor que indica la posición inicial de la secuencia, que pertenece a los naturales.
            e, valor que indica la posición final de la secuencia, que pertenece a los naturales.
@outputs:   valor que contiene la suma de la secuencia desde la posición b hasta la e, que pertenece a los reales.
'''
def sum(S,b,e):
    if(b>e):
        return 0
    else:
        q = math.floor((b+e)/2)
        return sum(S,b,q-1) + S[q] + sum(S,q+1,e)
    # end if
# end def sum

## -------------------------------------------------------------------------
'''
maxDiffermCross: Invoca la función maxDiffermAux para encontrar la i-diferencia máxima con "dividir-y-vencer"
@inputs:    S, secuencia de elementos, que pertenece a los reales.
            b, valor que indica la posición inicial de la secuencia, que pertenece a los naturales.
            q, valor que indica el pivote, que pertenece a los naturales
            e, valor que indica la posición final de la secuencia, que pertenece a los naturales.
@outputs:   valor que contiene el resultado de la resta entre los elementos por el lado izquierdo y derecho del pivote.
'''
def maxDiffermCross(S,b,q,e):
    return [(sum(S,b,q-1) - sum(S,q+1,e)),q]
# end def maxDiffermCross

## -------------------------------------------------------------------------
'''
maxDifferm_C1: Invoca la función maxDiffermAux para encontrar la i-diferencia máxima con "dividir-y-vencer"
@inputs:    S, secuencia de elementos, que pertenece a los reales.
@outputs:   difference, valor máximo i-diferencia de la secuencia, que pertenece a los reales.
            pos, valor de la posición de la máxima i-diferencia de la secuencia, que pertenece a los naturales.
'''
def maxDifferm_C1(S):
    return maxDiffermAux(S,0,len(S)-1)
# end def maxDifferm_C1

## -------------------------------------------------------------------------
'''
maxDiffermAux: Encuentra la i-diferencia máxima con "dividir-y-vencer"
@inputs:    S, secuencia de elementos, que pertenece a los reales.
            b, valor que indica la posición inicial de la secuencia, que pertenece a los naturales.
            e, valor que indica la posición final de la secuencia, que pertenece a los naturales.
@outputs:   difference, valor máximo i-diferencia de la secuencia, que pertenece a los reales.
            pos, valor de la posición de la máxima i-diferencia de la secuencia, que pertenece a los naturales.
'''
def maxDiffermAux(S,b,e):
    if(b>e):
        return [-math.inf,-1]
    else:
        q = math.floor((b+e)/2)
        [diffC,posC] = maxDiffermCross(S,0,q,len(S)-1)
        [diffL,posL] = maxDiffermAux(S,b,q-1)
        [diffR,posR] = maxDiffermAux(S,q+1,e)
        if ((diffL > diffR) and (diffL > diffC)):
            return [diffL,posL]
        elif((diffR > diffL) and (diffR > diffC)):
            return [diffR,posR]
        else:
            return [diffC,posC]
        # end if
    # end if
# end def maxDiffermAux

## eof - codigo.py