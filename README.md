# **TALLER 2 - DIVIDIR Y VENCER** <br/>
Máxima i-diferencia de una secuencia <br/>

## Presentado por :star2:

* Juan Sebastián Barreto Jiménez
* Janet Chen He

## Objetivo Parte 2
Implementar un algoritmo basado en la estrategia "dividir-y-vencer". Esta entrega cubre la mitad del taller 2.

## Descripción Parte 2
En la entrega anterior usted escribió dos algoritmos (uno iterativo y otro basado en "dividir-y-vencer") para solucionar el problema "encontrar la máxima i-diferencia de una secuencia".

En esta entrega usted debe implementar el algoritmo "dividir-y-vencer" en lenguaje Python. Esa implementación será evaluada de dos formas:

Reporte de pruebas con 30000 casos de entrada diferentes: a partir de esa experimentación, confirmar o refutar el análisis de orden de complejidad hecho en la entrega anterior.
Comparar los resultados a partir de estos datos de entrada con estos datos esperados de salida. Discutir la comparación entre lo que su implementación genera contra los datos esperados.

## Estructura de Archivos
1. codeOne.py -> Contiene la implementación de Dividir y Vencer del caso 1.
2. codeTwo.py -> Contiene la implementación de Dividir y Vencer del caso 2.
3. plots.text -> Contiene los resultados de la gráfica de las pruebas del Dividir y Vencer del caso 1 hasta 30000.
4. plotsTwo.text -> Contiene los resultados de la gráfica de las pruebas del Dividir y Vencer del caso 1 y 2 hasta 990.
5. test_cases.in -> Archivo de entrada.
6. test_cases.out -> Archivo de salida.
7. test.py -> Pruebas de la implementación de Dividir y Vencer del caso 1.
8. testTwo.py -> Pruebas de entrada y salida de la implementación de Dividir y Vencer del caso 1.
9. testTwoCodes.py ->  Pruebas de la implementación de Dividir y Vencer del caso 1 y 2.
10. plotTest.sh -> Script que saca la gráfica de la implementación de Dividir y Vencer del caso 1 con 30000 datos.
11. plotTestTwoCodes.sh -> Script que saca la gráfica de la implementación de Dividir y Vencer del caso 1 y 2 con 990 datos.
